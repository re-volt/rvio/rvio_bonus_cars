Car information
================================================================
Car name: Pest Buster
Car Type: Remodel
Top speed: 57 kph
Rating/Class: 2 (Advanced)
I/O Class: Prugs
Installed folder: ...\cars\pestbuster
Description: 

While driving some laps with Pest Control, I had this idea to do a pest exterminator company
van. It's in the same class then Pest Control, but I'm sure you will like the handling more! ;)

For the body I took Badd RC as a basis, and did some small adjustments. The art design is
from original Pest Control. It was my purpose, to let it look the same, so please don't
complain about this.

After adding the spider on the top, I was searching for an idea, to make the whole thing a
little bit funnier. I liked the idea from pmjar.prj to turn the rotating spider into a
dead spider. Finally I added some green spider blood, flooding all over the roof and the
side parts of the van.

!!! ATTENTION !!!: If you are a sensitive and spider-loving person, better don't download
Pest Buster. :D

Texture and boxart size is matching the classical Re-Volt style (256x256 for texture), if
you like to have it in better quality, higher resolution files are included.

Have fun with Pest Buster.

Author Information
================================================================
Author Name: Kiwi
Email Address: kiwi@rv.gl
 
Construction
================================================================
Base: Body: Badd RC (from Mighty Cucumber and URV); Texture: Pest Control (Acclaim Original)
Editor(s) used: Notepad++, Blender (with HabitatB- and ReVolt-Plugin from Marv), PhotoImpact12
 
Additional Credits 
================================================================
+ Thanks to the guys at RVIO community in Discord (#creations) for your help, suggestions
and compliments!
+ Shara for the name idea
+ pmjar.prj for the upside-down spider idea


Copyright / Permissions
================================================================
Please do not change any parts of this car without asking, as long it's not for your personal use.
You can reuse any parts of this car for your own car, as long you mention the
original creators accordingly.

Version 2.2 from March 2nd, 2020