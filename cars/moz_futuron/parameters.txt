{

;============================================================
;============================================================
; Futuron
;============================================================
;============================================================
Name       	"Futuron"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	2 			; Skill level (rookie, amateur, ...)
TopEnd     	3096.903809 		; Actual top speed (mph) for frontend bars
Acc        	7.587869 		; Acceleration rating (empirical)
Weight     	1.700000 		; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 		; Max Revs (for rev counter, deprecated...)

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/moz_futuron/body.prm"
MODEL 	1 	"cars/moz_futuron/wheelll.prm"
MODEL 	2 	"cars/moz_futuron/wheelrr.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"cars/moz_futuron/axlell.prm"
MODEL 	10 	"cars/moz_futuron/axlerr.prm"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 		"cars/moz_futuron/hull.hul"
TPAGE 		"cars/moz_futuron/car.bmp"
;)TCARBOX 	"cars/moz_futuron/carbox.bmp" 			; Carbox texture
;)TSHADOW 	"cars/moz_futuron/shadow.bmp" 			; Shadow texture
;)SHADOWINDEX 	-1 							; Use a default shadow (0 to 27 or -1)
;)SHADOWTABLE 	-64.500000 64.500000 66.500000 -66.000000 -1.500000 	; Left, right, front, back, height (relative to model center)
;)SFXENGINE 	"NONE"
;)SFXSERVO 	"NONE"
;)SFXHONK 	"NONE"
EnvRGB 		100 100 100

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	5.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	37.000000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -8.000000 1.000000 	; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 	; Weapon generation offset
;)Flippable	false 		; Rotor car effect
;)Flying   	false 		; Flying like the UFO car
;)ClothFx  	false 		; Mystery car cloth effect

;====================
; Camera details
;====================

;)CAMATTACHED {	; Start Camera
;)HoodOffset   	0.000000 0.000000 0.000000 	; Offset from model center
;)HoodLook     	0.050000 			; Look angle (-0.25 to 0.25, 0.0 - straight ahead)
;)RearOffset   	0.000000 0.000000 0.000000
;)RearLook     	0.050000
;)FixedOffset  	true 				; Is offset fixed or moving
;)FixedLook    	true 				; Is look fixed or moving
;)UseDefault   	true 				; Use default offsets (computed in game)
;)}            	; End Camera

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	1.700000
Inertia    	1200.000000 0.000000 0.000000
           	0.000000 2000.000000 0.000000
           	0.000000 0.000000 1600.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 		; Linear air resistance
AngRes     	0.001000 		; Angular air resistance
ResMod     	25.000000 		; AngRes scale when in air
Grip       	0.015000 		; Converts downforce to friction value
StaticFriction 	0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-30.000000 2.000000 46.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.500000
EngineRatio 	12350.000000
Radius      	13.800000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	2.000000
SkidWidth   	8.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.030000
Grip            0.020000
StaticFriction  1.750000
KineticFriction 1.650000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	30.000000 2.000000 46.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.500000
EngineRatio 	12350.000000
Radius      	13.800000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	2.000000
SkidWidth   	8.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.030000
Grip            0.020000
StaticFriction  1.750000
KineticFriction 1.650000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-30.000000 2.000000 -47.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	12350.000000
Radius      	13.800000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	2.000000
SkidWidth   	8.500000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.040000
Grip            0.020000
StaticFriction  1.750000
KineticFriction 1.650000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	30.000000 2.000000 -47.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	12350.000000
Radius      	13.800000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	2.000000
SkidWidth   	8.500000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.040000
Grip            0.020000
StaticFriction  1.750000
KineticFriction 1.650000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	-8.000000 -10.000000 38.000000
Length      	15.000000
Stiffness   	780.000000
Damping     	13.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	8.000000 -10.000000 38.000000
Length      	15.000000
Stiffness   	780.000000
Damping     	13.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	-9.000000 -15.000000 -35.000000
Length      	14.000000
Stiffness   	780.000000
Damping     	13.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	9.000000 -15.000000 -35.000000
Length      	14.000000
Stiffness   	780.000000
Damping     	13.000000
Restitution 	-0.950000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	9
Offset      	-10.000000 0.000000 46.000000
Length      	21.200001
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	10
Offset      	10.000000 0.000000 46.000000
Length      	21.200001
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	9
Offset      	-10.000000 0.000000 -47.000000
Length      	21.200001
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	10
Offset      	10.000000 0.000000 -47.000000
Length      	21.200001
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
;)Type      	1 				; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 	; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-17.000000 -18.000000 -30.000000
Direction   	0.000000 -1.000000 -0.800000
Length      	17.000000
Stiffness   	8000.000000
Damping     	8.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	; Start AI
UnderThresh 	5.000000
UnderRange  	1584.415039
UnderFront  	401.940002
UnderRear   	321.519989
UnderMax    	0.283685
OverThresh  	1954.575073
OverRange   	1541.581543
OverMax     	1.000000
OverAccThresh  	131.029999
OverAccRange   	1928.010010
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	26213
Aggression     	0
}           	; End AI

}

