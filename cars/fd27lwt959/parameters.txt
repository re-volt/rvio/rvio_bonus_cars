{

;============================================================
;============================================================
; Acid Strike
;============================================================
;============================================================
Name       	"Acid Strike"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	3 			; Skill level (rookie, amateur, ...)
TopEnd     	3893.679199 			; Actual top speed (mph) for frontend bars
Acc        	7.527932 			; Acceleration rating (empirical)
Weight     	1.800000 			; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 			; Max Revs (for rev counter, deprecated...)
;)TCARBOX  	"cars/fd27lwt959/carbox.bmp" 		; Carbox texture

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/fd27lwt959/body.prm"
MODEL 	1 	"cars/fd27lwt959/wheel-l.prm"
MODEL 	2 	"cars/fd27lwt959/wheel-r.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"cars/fd27lwt959/spring.prm"
MODEL 	9 	"cars/fd27lwt959/axle.prm"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 	"cars/fd27lwt959/hull.hul"
TPAGE 	"cars/fd27lwt959/car.bmp"
;)TSHADOW 	"cars/fd27lwt959/shadow.bmp"
;)SHADOWINDEX 	-1
;)SHADOWTABLE 	-58.400002 58.400002 74.300003 -86.500000 -11.500000 	; Left, right, front, back, height (relative to model center)
EnvRGB 	200 200 200

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	50.389998 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 7.000000 0.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon generation offset
;)Flippable	false 			; Rotor car effect
;)Flying   	false 			; Flying like the UFO car
;)ClothFx  	false 			; Mystery car cloth effect

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	1.800000
Inertia    	1840.000000 0.000000 0.000000
           	0.000000 2010.000000 0.000000
           	0.000000 0.000000 280.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air resistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	30.000000 			; AngRes scale when in air
Grip       	0.070000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-24.000000 -1.000000 44.000000
Offset2  	-4.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.540000
EngineRatio 	14000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	8.000000
SkidWidth   	18.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.010000
Grip            	0.027000
StaticFriction  	1.350000
KineticFriction 	1.250000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	24.000000 -1.000000 44.000000
Offset2  	4.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.540000
EngineRatio 	14000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	8.000000
SkidWidth   	18.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.010000
Grip            	0.027000
StaticFriction  	1.350000
KineticFriction 	1.250000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-24.000000 -1.000000 -45.000000
Offset2  	-6.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	14000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	8.000000
SkidWidth   	18.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.010000
Grip            	0.027000
StaticFriction  	1.350000
KineticFriction 	1.250000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	24.000000 -1.000000 -45.000000
Offset2  	6.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	14000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	8.000000
SkidWidth   	18.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.010000
Grip            	0.027000
StaticFriction  	1.350000
KineticFriction 	1.250000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	8
Offset      	-10.000000 -21.000000 43.500000
Length      	15.000000
Stiffness   	200.000000
Damping     	7.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	8
Offset      	10.000000 -21.000000 43.500000
Length      	15.000000
Stiffness   	200.000000
Damping     	7.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	8
Offset      	-10.000000 -21.000000 -42.500000
Length      	15.000000
Stiffness   	200.000000
Damping     	7.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	8
Offset      	10.000000 -21.000000 -42.500000
Length      	15.000000
Stiffness   	200.000000
Damping     	7.000000
Restitution 	-0.750000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	9
Offset      	0.000000 -8.000000 43.000000
Length      	18.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	9
Offset      	0.000000 -8.000000 43.000000
Length      	18.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	9
Offset      	0.000000 -8.000000 -46.000000
Length      	18.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	9
Offset      	0.000000 -8.000000 -46.000000
Length      	18.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
;)Type      	1 			; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 		; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-15.000000 -22.000000 -30.000000
Direction   	0.000000 -1.000000 0.000000
Length      	30.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	1562.777832
UnderRange  	1104.010498
UnderFront	 	831.707153
UnderRear   	335.000000
UnderMax    	0.950000
OverThresh  	2685.659424
OverRange   	2319.498291
OverMax     	0.901331
OverAccThresh  	10.000000
OverAccRange   	1794.701904
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

