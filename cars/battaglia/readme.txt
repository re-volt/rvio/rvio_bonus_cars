Car information
================================================================
Car name                : Battaglia
Car Type  		: Original
Top speed 		: 63 kph (51 kph Average)
Rating/Class   		: 3 (semi-pro)
Installed folder       	: ...\cars\battaglia
Description             : 

A Battle Machine with extreme resilience

Battaglia is a sports truck made with the battlefield in mind.
the car's weight exceeds the usual scale because of the large engine on its back.
no performance is compromised in the goal of creating a Battle Machine.

its large engine improves its weight for resilience while generating lots of power.
the body's slanted design improves its aerodynamics, assisting in speed and acceleration.
its soft suspension setup and high ride height allows the car to drive on any terrain unhindered.

Battaglia is made to be very durable and resilient, able to take on any terrains and crash other cars with ease.
its durability and weight prevents the car from being pushed and able to resist pickups, with zap and oils having less effect on this machine.
this car has semi-pro performance. however, due to its extreme durability and ease of use, this car can compete against pros, although requires intentional crashing attempts to keep up.

The outrageous resilience, uncompromised performance and terrain adaptability makes Battaglia to be one of the most formidable Battle Machine available.

Author Information
================================================================
Author Name 		: shara coronvi
Email Address           : sharacolonvee@mail.com
Other Info		: pretty much a recent Re-Volt veteran. 
                          a good racer and drifter. experienced in parametering and repaints.
                          creator of aerodynamic car designs. also a drawing "artist" too.
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Notepad, Paint.NET, SketchUp and Blender
 
Additional Credits 
================================================================
Thanks and Credits to those who inspired or helped.

Acclaim for this great game
Microsoft for Notepad
Paint.NET developers for the afromentioned program
Marv for the Blender plugin
Xarc for paintjob, hull, and carbox

Copyright / Permissions
================================================================
If you're considering to put this car in a pack, you may NOT modify the car's handling.
if the car handling doesnt fit into the theme of the pack (ie too slow, too fast), simply dont put this car in.
though, you are ALLOWED to make a NEW car using this as a base, and then put that new car into a pack.

Due to the new skin selection system. Standalone repaints of this car is not allowed, even if it has modified params.
Above rule is to prevent this original creation from being overshadowed by any new potential repaints of this car.
Repaints of this car can only be released as skins for this car's specific foldername, ask the author if you want your skin to be published and bundled with the car.

Remodels and Originals based on this car are allowed without further limits.
Remapping the UV map without changing the body geometry for repainting doesnt count as Remodel, therefore not allowed.
Other than those said above, you may do whatever you want with this car.
