{

;============================================================
;============================================================
; Repaint by MightyCucumber
;============================================================
;============================================================
Name      	"Misterio"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\Mist\body.prm"
MODEL 	1 	"cars\Mist\wheelfl.prm"
MODEL 	2 	"cars\Mist\wheelfr.prm"
MODEL 	3 	"cars\Mist\wheelbl.prm"
MODEL 	4 	"cars\Mist\wheelbr.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 	"cars\Mist\car.bmp"
;)TCARBOX "cars\Mist\carbox.bmp"
TSHADOW "cars\Mist\shadow.bmp"
SHADOWTABLE -81.0 81.0 89.0 -68.0 -2.85
COLL 	"cars\Mist\hull.hul"
EnvRGB 	200 200 200

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	3 			; Skill level (rookie, amateur, ...)
TopEnd     	3375.636963 			; Actual top speed (mph) for frontend bars
Acc        	5.535534 			; Acceleration rating (empirical)
Weight     	1.500000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	0 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.5000000 			; Max Revs (for rev counter)
;) Statistics   TRUE

;====================
; Handling related stuff
;====================

SteerRate  	2.500000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.700000 			; Rate at which Engine voltage approaches set value
TopSpeed   	37.500000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 0.000000 0.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, -20, 10 		; Calculated in game
Mass       	1.500000
Inertia    	1000.000000 0.000000 0.000000
           	0.000000 1700.000000 0.000000
           	0.000000 0.000000 560.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001500 			; Linear air esistance
AngRes     	0.001500 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-31.000000 -3.000000 44.000000
Offset2  	-2.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.500000
EngineRatio 	12000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	10.000000
ToeIn       	1.000000
AxleFriction    	0.020000
Grip            	0.015000
StaticFriction  	1.800000
KineticFriction 	1.650000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	31.000000 -3.000000 44.000000
Offset2  	2.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.500000
EngineRatio 	12000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	10.000000
ToeIn       	1.000000
AxleFriction    	0.020000
Grip            	0.015000
StaticFriction  	1.800000
KineticFriction 	1.650000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-31.000000 -3.000000 -36.500000
Offset2  	-2.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	12000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	10.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.015000
StaticFriction  	1.880000
KineticFriction 	1.670000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	31.000000 -3.000000 -36.500000
Offset2  	2.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	12000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	10.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.015000
StaticFriction  	1.880000
KineticFriction 	1.670000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.750000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.750000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	30.000000 -20.000000 40.000000
Direction   	0.000000 -1.000000 0.000000
Length      	29.000000
Stiffness   	2800.000000
Damping     	5.000000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	150.000000
UnderRange  	3098.658691
UnderFront	 	2248.433105
UnderRear   	1541.193848
UnderMax    	0.320000
OverThresh  	1713.217529
OverRange   	1831.650024
OverMax     	0.900000
OverAccThresh  	89.169998
OverAccRange   	400.000000
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

C6E1F417