--INFERNO MP777 ---------

-- CAR INFO -------------

Name: Inferno MP777
Creator: Hi-Ban
Car Type: Original
Engine Class: Glow
Rating: Pro
Top Speed: 40 mph (64km/h)

-- FEATURES -------------

- Custom Body mesh.
- Custom Spring mesh.
- Custom Wheel meshes.
- Custom Axle mesh.
- Custom Textures.
- Custom Hull.
- Custom Parameters.
- Custom Car Box.
- Custom Shadow.

-- NOTES ----------------

I took a real RC car (Kyosho Inferno MP777) as reference for making this.
The aim was to build a detailed car, while still fitting with the visual style of the stock cars.

I also did this due to the lack of custom buggys available for re-volt.

The parameters have been tweaked to be balanced with the stock Pro cars.

You can freely make a repaint, or use parts of this car (springs, wheels...) as long as you credit me accordingly in your readme.

-- CONTACT --------------

hi-ban@hotmail.com
