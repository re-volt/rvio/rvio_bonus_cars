﻿Car information
================================================================
Car name                : SchoolVolt
Car Type  		: Original
Top speed 		: 31.6 mph
Rating/Class   		: Rookie
Installed folder       	: ...\cars\fd26school

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.