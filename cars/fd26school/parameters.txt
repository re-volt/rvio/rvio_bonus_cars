{

;============================================================
;============================================================
; SchoolVolt
;============================================================
;============================================================
Name       	"SchoolVolt"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	0 			; Skill level (rookie, amateur, ...)
TopEnd     	2830.819092 			; Actual top speed (mph) for frontend bars
Acc        	7.135937 			; Acceleration rating (empirical)
Weight     	1.800000 			; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 			; Max Revs (for rev counter, deprecated...)
;)TCARBOX  	"cars/fd26school/carbox.bmp" 		; Carbox texture

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/fd26school/body.prm"
MODEL 	1 	"cars/fd26school/wheel-l.prm"
MODEL 	2 	"cars/fd26school/wheel-r.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 	"cars/fd26school/hull.hul"
TPAGE 	"cars/fd26school/car.bmp"
;)TSHADOW 	"cars/fd26school/shadow.bmp"
;)SHADOWINDEX 	-1
;)SHADOWTABLE 	-46.599998 46.400002 96.699997 -88.300003 -10.700000 	; Left, right, front, back, height (relative to model center)
EnvRGB 	200 200 200

;====================
; Handling related stuff
;====================

SteerRate  	2.600000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	33.500000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 2.000000 -15.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon generation offset
;)Flippable	false 			; Rotor car effect
;)Flying   	false 			; Flying like the UFO car
;)ClothFx  	false 			; Mystery car cloth effect

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	1.800000
Inertia    	3180.000000 0.000000 0.000000
           	0.000000 3210.000000 0.000000
           	0.000000 0.000000 530.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air resistance
AngRes     	0.004000 			; Angular air resistance
ResMod     	25.000000 			; AngRes scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 1.000000
KineticFriction 0.700000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-23.500000 7.000000 57.000000
Offset2  	-2.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.300000
EngineRatio 	11000.000000
Radius      	12.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	13.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.020000
Grip            	0.012000
StaticFriction  	1.500000
KineticFriction 	1.100000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	23.500000 7.000000 57.000000
Offset2  	2.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.300000
EngineRatio 	11000.000000
Radius      	12.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	13.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.020000
Grip            	0.012000
StaticFriction  	1.500000
KineticFriction 	1.100000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-25.200001 7.000000 -24.000000
Offset2  	-2.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.000000
EngineRatio 	11000.000000
Radius      	12.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	13.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.050000
Grip            	0.012000
StaticFriction  	1.500000
KineticFriction 	1.100000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	25.200001 7.000000 -24.000000
Offset2  	2.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.000000
EngineRatio 	11000.000000
Radius      	12.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	13.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.050000
Grip            	0.012000
StaticFriction  	1.500000
KineticFriction 	1.100000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	-17.000000 -26.000000 39.000000
Length      	13.000000
Stiffness   	200.000000
Damping     	2.000000
Restitution 	-0.800000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	17.000000 -26.000000 39.000000
Length      	13.000000
Stiffness   	200.000000
Damping     	2.000000
Restitution 	-0.800000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	-17.000000 -26.000000 -33.000000
Length      	13.000000
Stiffness   	200.000000
Damping     	2.000000
Restitution 	-0.800000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	17.000000 -26.000000 -33.000000
Length      	13.000000
Stiffness   	200.000000
Damping     	2.000000
Restitution 	-0.800000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	15.000000 -10.000000 38.000000
Length      	18.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	-15.000000 -10.000000 38.000000
Length      	18.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	15.000000 -10.000000 -38.000000
Length      	18.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	-15.000000 -10.000000 -38.000000
Length      	18.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
;)Type      	1 			; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 		; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-27.000000 -27.000000 40.000000
Direction   	0.000000 -1.000000 0.000000
Length      	25.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	630.643433
UnderRange  	1094.642822
UnderFront  	1778.746338
UnderRear   	738.563232
UnderMax    	0.950000
OverThresh  	517.576172
OverRange   	1391.000000
OverMax     	0.962583
OverAccThresh  	348.559540
OverAccRange   	420.339996
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

