August 15, 2017: Readme file of Alien Buster.

================================================================
======================== Car Info ==============================
================================================================

Name				Alien Buster
Authors				Allan1
Date				March 01, 2017
Type				Original
Folder name			alienbuster
Class				Glow
Rating				Semi-Pro
Top speed			42 mph
Drivetrain			Four-wheel Drive
Body mass			2 kg


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory. Files will be automatically placed on the correct folders.


================================================================
===================== Lore/Description =========================
================================================================

"In an isolated western town, rumors of the appearance of aliens spread throughout the region. Strange phenomena began to happen, and reports of witnesses reached everyone's ear. At some point, aliens become a plague, and the risk of this catastrophe spreading in other cities was great. Who is going to defend us?"

The "Alien Buster" was a mix of various 1950s vintage pickups designs. I wanted a really rough car, and an old pickup truck came in handy. The idea of a small group interested in defending the vicinity of aliens using a rustic equipment came to mind, I think it was from a movie I watched a long time ago.

Handling resembles BossVolt a little, since the base parameters were its. I made several changes to stay the way I wanted to, and here it is!


================================================================
======================== Credits ===============================
================================================================

Alien face, sniper view and radar were borrowed from internet, all labeled for reuse with modification.

Track in the picture is Cactus Volt by Killer Wheels.

Used Tools:
Autodesk 3D Studio Max
Asetools (by Ali)
GetUV (by Kay)
Adobe Photoshop CS2
noPureBlack (by Kay)
Inkscape
RVGL (by Huki)
MS Notepad
Prm2Hul (by Jig)


================================================================
======================== Changelog =============================
================================================================

August 15, 2017:
-Fixed body AngRes
-Decreased weight
-Changed spring values
-Fixed shadow bug after jump
-Rebalanced car to fit better with the other semi-pros


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan