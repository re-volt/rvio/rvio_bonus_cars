{

;============================================================
;============================================================
; Starliner
;============================================================
;============================================================
Name      	"Starliner"


;====================
; Models
;====================

MODEL 	0 	"cars\starliner\body.prm"
MODEL 	1 	"cars\starliner\wheel-l.prm"
MODEL 	2 	"cars\starliner\wheel-r.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"cars\starliner\1.m"
MODEL 	16 	"cars\starliner\2.m"
MODEL 	17 	"NONE"
MODEL 	18 	"NONE"
TPAGE 	"cars\starliner\car.bmp"
;)TCARBOX "cars\starliner\carbox.bmp"
;)TSHADOW "cars\starliner\shadow.bmp"
;)SHADOWTABLE -100.4718 100.4718 98.1663 -98.1663 -15.0664
COLL 	"cars\starliner\hull.hul"
;)SFXENGINE "cars\starliner\petrol.wav"
EnvRGB 	30 30 30

;====================
; Frontend
;====================

BestTime   	TRUE
Selectable 	TRUE
;)Statistics	TRUE
Class      	1
Obtain     	0
Rating     	2
TopEnd     	2915.862305
Acc        	6.596510
Weight     	2.000000
Handling   	50.000000
Trans      	0
MaxRevs    	0.500000

;====================
; Handling
;====================

SteerRate  	1.750000
SteerMod   	0.000000
EngineRate 	2.800000
TopSpeed   	34.000000
DownForceMod	0.700000
CoM        	0.000000 -14.000000 5.000000
Weapon     	0.000000 -32.000000 64.000000

;====================
; Body
;====================

BODY {		; Start Body
ModelNum   	0
Offset     	0, 0, 0
Mass       	2.000000
Inertia    	1150.000000 0.000000 0.000000
           	0.000000 2050.000000 0.000000
           	0.000000 0.000000 600.000000
Gravity		2200
Hardness   	0.000000
Resistance 	0.001000
AngRes     	0.001000
ResMod     	25.000000
Grip       	0.020000
StaticFriction  0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Wheels
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-23.000000 12.050000 50.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.300000
EngineRatio 	20000.000000
Radius      	12.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	7.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.005500
StaticFriction  	1.200000
KineticFriction 	1.200000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	23.000000 12.050000 50.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.300000
EngineRatio 	20000.000000
Radius      	12.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	7.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.005500
StaticFriction  	1.200000
KineticFriction 	1.200000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-23.000000 12.050000 -35.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.100000
EngineRatio 	10000.000000
Radius      	12.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	7.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.005500
StaticFriction  	1.250000
KineticFriction 	1.220000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	23.00000 12.050000 -35.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.100000
EngineRatio 	10000.000000
Radius      	12.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	7.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.005500
StaticFriction  	1.250000
KineticFriction 	1.220000
}          	; End Wheel


;====================
; Springs
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	600.000000
Damping     	12.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	600.000000
Damping     	12.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	600.000000
Damping     	12.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	600.000000
Damping     	12.000000
Restitution 	-0.900000
}           	; End Spring


;====================
; Pins
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Axles
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Spinner
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 0.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Aerial
;====================

AERIAL {    	; Start Aerial
SecModelNum 	15
TopModelNum 	16
Offset      	-17.000000 -16.000000 -55.000000
Direction   	0.000000 -1.000000 0.000000
Length      	16.000000
Stiffness   	3500.000000
Damping     	15.000000
}           	; End Aerial


;====================
; AI
;====================

AI {        	 ;Start AI
UnderThresh 	1043.510742
UnderRange  	2702.000000
UnderFront	2036.469604
UnderRear   	335.000000
UnderMax    	0.638043
OverThresh  	531.036438
OverRange   	1827.135498
OverMax     	0.640206
OverAccThresh  	613.148560
OverAccRange   	1021.179871
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI