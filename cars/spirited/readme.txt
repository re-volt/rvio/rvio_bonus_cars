Information
-----------------
Car Name:	Spirited
Rating:		Pro
Top speed:	41.0 mph
Acceleration:	3.05 m/s�
Weight:		1.7 kg
Author:		Saffron


Description
-----------------
To prevent Steezy's params from going to waste, I transferred them to a new car mostly based off of the Lotus Esprit GT1. The stats are the same, the handling is similiar, but the dimensions of the car, and as a result, its different wheel offsets will make it ever so slightly different to drive.

The paintjob is also inspired by one of Steezy's skins.

Requirements
-----------------
You MUST use the latest RVGL patch for the textures to display properly.


Credits
-----------------
Polyphony Digital for the model this car was based off of
Norfair for better or worse, the inspiration for the car
Phimeek and everyone in the Re-Volt Discord for some of the decals and textures
The Blender Foundation for Blender
Marv, Martin and Huki for the Blender plug-in
Jigebren for PRM2HUL
The RVGL Team for RVGL
Rick Brewster for Paint.NET


Permission
-----------------
My creations cannot be distributed in any content pack associated with RVON (Re-Volt Online).
User created skins for my cars cannot be added to content packs without my permission.
If you want to make modifications to a track of mine for special events and tournaments, do it under a different name and folder name, alongside specifying said modifications in a separate Readme file.
If you want to convert a creation of mine to another game, make sure to credit me.
If you make a car derived from this one (a repaint or a remodel), the permissions for that car must match this one's permissions.