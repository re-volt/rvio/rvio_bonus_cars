{

;============================================================
;============================================================
; Melon Crusher
;============================================================
;============================================================
Name       	"Melon Crusher"

;====================
; Models
;====================

MODEL	0 	"cars\meloncrusher\body.prm"
MODEL	1 	"cars\meloncrusher\wheell.prm"
MODEL	2 	"cars\meloncrusher\wheelr.prm"
MODEL	3 	"NONE"
MODEL	4 	"NONE"
MODEL	5 	"NONE"
MODEL	6 	"NONE"
MODEL	7 	"cars\meloncrusher\options.prm"
MODEL	8 	"cars\meloncrusher\spring.prm"
MODEL	9 	"cars\meloncrusher\axle.prm"
MODEL	10 	"NONE"
MODEL	11 	"NONE"
MODEL	12 	"NONE"
MODEL	13 	"NONE"
MODEL	14 	"NONE"
MODEL	15 	"NONE"
MODEL	16 	"NONE"
MODEL	17 	"cars\misc\Aerial.m"
MODEL	18 	"cars\misc\AerialT.m"
TPAGE		"cars\meloncrusher\car.bmp"
;)TCARBOX	"cars\meloncrusher\carbox.bmp"
;)TSHADOW	"cars\meloncrusher\shadow.bmp"
;)SHADOWTABLE	-75.000000 75.000000 95.000000 -85.000000 -10.000000
COLL		"cars\meloncrusher\hull.hul"
;)SFXENGINE	"cars\meloncrusher\engine.wav"
;)SFXHONK	"cars\meloncrusher\honk.wav"
EnvRGB		100 100 100

;====================
; Frontend
;====================

BestTime   	TRUE
Selectable 	TRUE
;)Statistics 	TRUE
Class      	1
Obtain     	0
Rating     	2
TopEnd     	3343.240967
Acc        	8.530000
Weight     	2.720000
Trans      	2
MaxRevs    	0.500000

;====================
; Handling
;====================

SteerRate  	8.000000
SteerMod   	0.000000
EngineRate 	10.000000
TopSpeed   	39.000000
DownForceMod	2.000000
CoM        	0.000000 3.000000 -3.000000
Weapon     	0.000000 -32.000000 64.000000

;====================
; Body
;====================

BODY {
ModelNum   	0
Offset     	0.000000 0.000000 0.000000
Mass       	2.720000
Inertia    	7000.000000 0.000000 0.000000
		0.000000 12200.000000 0.000000
		0.000000 0.000000 4250.000000
Gravity    	2200
Hardness   	0.000000
Resistance 	0.001000
AngRes     	0.001000
ResMod     	25.000000
Grip       	0.010000
StaticFriction 	0.800000
KineticFriction 0.400000
}

;====================
; Wheels
;====================

WHEEL 0 {
ModelNum 	1
Offset1  	-35.000000 1.000000 47.000000
Offset2  	-10.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.300000
EngineRatio 	0.000000
Radius      	21.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	30.000000
SkidWidth   	20.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.018000
StaticFriction  	1.800000
KineticFriction 	1.720000
}

WHEEL 1 {
ModelNum 	2
Offset1  	35.000000 1.000000 47.000000
Offset2  	10.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.300000
EngineRatio 	0.000000
Radius      	21.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	30.000000
SkidWidth   	20.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.018000
StaticFriction  	1.800000
KineticFriction 	1.720000
}

WHEEL 2 {
ModelNum 	1
Offset1  	-35.000000 1.000000 -41.000000
Offset2  	-10.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.020000
EngineRatio 	57500.000000
Radius      	21.250000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	30.000000
SkidWidth   	20.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.050000
Grip            	0.018000
StaticFriction  	1.770000
KineticFriction 	1.690000
}

WHEEL 3 {
ModelNum 	2
Offset1  	35.000000 1.000000 -41.000000
Offset2  	10.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.020000
EngineRatio 	57500.000000
Radius      	21.250000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	30.000000
SkidWidth   	20.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.018000
StaticFriction  	1.770000
KineticFriction 	1.690000
}

;====================
; Springs
;====================

SPRING 0 {
ModelNum    	8
Offset      	-20.000000 -30.000000 45.000000
Length      	15.500000
Stiffness   	200.000000
Damping     	5.000000
Restitution 	-0.900000
}

SPRING 1 {
ModelNum    	8
Offset      	20.000000 -30.000000 45.000000
Length      	15.500000
Stiffness   	200.000000
Damping     	5.000000
Restitution 	-0.900000
}

SPRING 2 {
ModelNum    	8
Offset      	-20.000000 -40.000000 -36.000000
Length      	15.500000
Stiffness   	200.000000
Damping     	5.000000
Restitution 	-0.900000
}

SPRING 3 {
ModelNum    	8
Offset      	20.000000 -40.000000 -36.000000
Length      	15.500000
Stiffness   	200.000000
Damping     	5.000000
Restitution 	-0.900000
}

;====================
; Pins
;====================

PIN 0 {
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}

PIN 1 {
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}

PIN 2 {
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}

PIN 3 {
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}

;====================
; Axles
;====================

AXLE 0 {
ModelNum    	9
Offset      	-5.000000 -12.000000 45.000000
Length      	20.000000
}

AXLE 1 {
ModelNum    	9
Offset      	5.000000 -12.000000 45.000000
Length      	20.000000
}

AXLE 2 {
ModelNum    	9
Offset      	-5.000000 -14.000000 -42.000000
Length      	20.000000
}

AXLE 3 {
ModelNum    	9
Offset      	5.000000 -14.000000 -42.000000
Length      	20.000000
}

;====================
; Spinner
;====================

SPINNER {
ModelNum    	7
Offset     	0.000000 0.000000 0.000000
Axis        	0.000000 0.000000 0.000000
AngVel      	0.000000
}

;====================
; Aerial
;====================

AERIAL {
SecModelNum 	17
TopModelNum 	18
Offset      	18.000000 -40.000000 38.000000
Direction   	0.000000 -1.000000 0.000000
Length      	35.000000
Stiffness   	2000.000000
Damping     	5.500000
}

;====================
; AI
;====================

AI {
UnderThresh 	1043.510742
UnderRange  	2702.099854
UnderFront  	2036.469604
UnderRear   	335.000000
UnderMax    	0.638043
OverThresh  	531.036438
OverRange   	1827.135498
OverMax     	0.640206
OverAccThresh  	613.148560
OverAccRange   	1021.179871
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}

2B1B3C5C