{

;============================================================
;============================================================
; Highway Mauler
;============================================================
;============================================================
Name 	"Highway Mauler"


;====================
; Model Filenames
;====================

MODEL 0 	"cars\phim_highmaul\body.prm"
MODEL 1 	"cars\phim_highmaul\wheel-fl.prm"
MODEL 2 	"cars\phim_highmaul\wheel-fr.prm"
MODEL 3 	"cars\phim_highmaul\wheel-mid.prm"
MODEL 4 	"cars\phim_highmaul\wheel-rear.prm"
MODEL 5 	"NONE"
MODEL 6 	"NONE"
MODEL 7 	"NONE"
MODEL 8 	"NONE"
MODEL 9 	"NONE"
MODEL 10 	"NONE"
MODEL 11 	"NONE"
MODEL 12 	"NONE"
MODEL 13 	"NONE"
MODEL 14 	"NONE"
MODEL 15 	"NONE"
MODEL 16 	"NONE"
MODEL 17 	"cars\misc\Aerial.m"
MODEL 18 	"cars\misc\AerialT.m"
TPAGE 	"cars\phim_highmaul\car.bmp"
COLL 	"cars\phim_highmaul\hull.hul"
;)TCARBOX "cars\phim_highmaul\box.bmp"
;)TSHADOW "cars\phim_highmaul\shadow.bmp"
;)SHADOWTABLE -98.5 98.5 89.0 -115.1 0
EnvRGB 	200 200 200


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime 	TRUE
Selectable 	TRUE
;)CPUSelectable TRUE
;)Statistics TRUE
Class 	1
Obtain 	0
Rating 	3
TopEnd 	3399.310741
Acc 	9.051803
Weight 	3.500000
Handling 	50.000000
Trans 	0
MaxRevs 	0.500000


;====================
; Handling related stuff
;====================

SteerRate 	3.000000
SteerMod 	0.400000
EngineRate 	4.500000
TopSpeed 	42.700000
DownForceMod 	2.000000
CoM 	0.000000 -1.000000 4.000000
Weapon 	0.000000 -32.000000 64.000000


;====================
; Car Body details
;====================

BODY { 	; Start Body
ModelNum 	0
Offset 	0, 0, 0
Mass 		3.500000
Inertia 	7948.000000 0.000000 0.000000
			0.000000 7830.000000 0.000000
			0.000000 0.000000 1530.000000
Gravity 	2200
Hardness 	0.000000
Resistance 	0.001000
AngRes 		0.004000
ResMod 		40.000000
Grip 		0.010000
StaticFriction 	1.000000
KineticFriction 	0.700000
} 	; End Body


;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1 	-0.470000 -4.000000 41.600000
Offset2 	-34.000000 0.000000 0.000000
IsPresent 			TRUE
IsPowered 			TRUE
IsTurnable 			TRUE
SteerRatio 			-0.183000
EngineRatio 		30000.000000
Radius 				18.000000
Mass 				0.200000
Gravity 			2100.000000
MaxPos 				8.000000
SkidWidth 			21.500000
ToeIn 				0.000000
AxleFriction 		0.020000
Grip 				0.010000
StaticFriction 		1.500000
KineticFriction 	1.500000
} 	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1 	0.470000 -4.000000 41.600000
Offset2 	34.000000 0.000000 0.000000
IsPresent 			TRUE
IsPowered 			TRUE
IsTurnable 			TRUE
SteerRatio 			-0.183000
EngineRatio 		30000.000000
Radius 				18.000000
Mass 				0.200000
Gravity 			2100.000000
MaxPos 				8.000000
SkidWidth 			21.500000
ToeIn 				0.000000
AxleFriction 		0.020000
Grip 				0.010000
StaticFriction 		1.500000
KineticFriction 	1.500000
} ; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1 	0.000000 -4.000000 -36.090000
Offset2 	-30.540000 0.000000 -18.960000
IsPresent 			TRUE
IsPowered 			TRUE
IsTurnable 			FALSE
SteerRatio 			0.000000
EngineRatio 		30000.000000
Radius 				18.000000
Mass 				0.200000
Gravity 			2200.000000
MaxPos 				8.000000
SkidWidth 			21.500000
ToeIn 				0.000000
AxleFriction 		0.050000
Grip 				0.011000
StaticFriction 		1.600000
KineticFriction 	1.600000
} 	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1 	0.0000000 -4.000000 -74.000000
Offset2 	30.5400000 0.000000 18.960000
IsPresent 			TRUE
IsPowered 			TRUE
IsTurnable 			FALSE
SteerRatio 			0.000000
EngineRatio 		30000.000000
Radius 				18.000000
Mass 				0.200000
Gravity 			2200.000000
MaxPos 				8.000000
SkidWidth 			21.500000
ToeIn 				0.000000
AxleFriction 		0.050000
Grip 				0.011000
StaticFriction 		1.600000
KineticFriction 	1.600000
} 	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum 	-1
Offset 		-19.900000 -25.890000 41.870000
Length 			31.000000
Stiffness 		400.00000
Damping 		9.000000
Restitution 	-0.950000
} 	; End Spring

SPRING 1 { 	; Start Spring
ModelNum 	-1
Offset 		19.900000 -25.890000 41.870000
Length 			31.000000
Stiffness 		400.000000
Damping 		9.000000
Restitution 	-0.950000
} 	; End Spring

SPRING 2 { 	; Start Spring
ModelNum 	-1
Offset 		-19.900000 -25.890000 -39.540000
Length 			31.000000
Stiffness 		400.000000
Damping 		9.000000
Restitution 	-0.950000
} 	; End Spring

SPRING 3 { 	; Start Spring
ModelNum 	-1
Offset 		19.900000 -25.890000 -39.540000
Length 			31.000000
Stiffness 		400.000000
Damping 		9.000000
Restitution 	-0.950000
} 	; End Spring


;====================
; Car Pin details
;====================

PIN 0 { 	; Start Pin
ModelNum 	-1
Offset 	0.000000 0.000000 0.000000
Length 	0.000000
} 	; End Pin

PIN 1 { 	; Start Pin
ModelNum 	-1
Offset 	0.000000 0.000000 0.000000
Length 	0.000000
} 	; End Pin

PIN 2 { 	; Start Pin
ModelNum 	-1
Offset 	0.000000 0.000000 0.000000
Length 	0.000000
} 	; End Pin

PIN 3 { 	; Start Pin
ModelNum 	-1
Offset 	0.000000 0.000000 0.000000
Length 	0.000000
} 	; End Pin


;====================
; Car axle details
;====================

AXLE 0 { 	; Start Axle
ModelNum 	-1
Offset 	-6.390000 -4.890000 41.820000
Length 	23.000000
} 	; End axle

AXLE 1 { 	; Start Axle
ModelNum 	-1
Offset 	6.390000 -4.890000 41.820000
Length 	23.000000
} 	; End axle

AXLE 2 { 	; Start Axle
ModelNum 	-1
Offset 	-6.390000 -4.890000 -39.360000
Length 	23.000000
} 	; End axle

AXLE 3 { 	; Start Axle
ModelNum 	-1
Offset 	6.390000 -4.890000 -39.360000
Length 	23.000000
} 	; End axle


;====================
; Car spinner details
;====================

SPINNER { 	; Start spinner
ModelNum 	-1
Offset 	0.000000 0.000000 0.000000
Axis 	0.000000 1.000000 0.000000
AngVel 	0.000000
} 	; End Spinner


;====================
; Car Aerial details
;==================== 

AERIAL { 	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset 		0.330000 -35.490000 -32.290000
Direction 	0.000000 -1.000000 0.000000
Length 		26.000000
Stiffness 	2000.000000
Damping 	5.500000
} 	; End Aerial


;====================
; Car AI details
;====================

AI { 	;Start AI
UnderThresh 	670.000000
UnderRange 		1
UnderFront 		0
UnderRear 		0
UnderMax 		-2.0
OverThresh 		670.000000
OverRange 		1
OverMax 		2.0
OverAccThresh 	5000
OverAccRange 	0
PickupBias 		13106
BlockBias 		22936
OvertakeBias 	19660
Suspension 		16383
Aggression 		0
} 	; End AI

}


