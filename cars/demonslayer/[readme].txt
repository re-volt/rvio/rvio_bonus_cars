
  D E M O N   S L A Y E R
  
  Version 1.0 from August 15th, 2021

================================================================
  Car information
================================================================

Date      : 15/08/2021
Car Name  : Demon Slayer
Author    : Kiwi
Car Type  : Remodel
Folder    : ...\cars\demonslayer
Top Speed : 32 mph
Mass      : 2,75 KG
Rating    : 1 (Amateur)

Demon Slayer is a Monster Truck version of Adeon, which I made for
the Re-Volt World Summer Contest 2021. The chassis and parameters
are based on Mighty's Get Air. The texture is based on one of my
Adeon skins named "Badeon". I kept the number plate B4D1, cause I
think it fit well for Demon Slayer as well. :)

Comes with a custom honk from the 1998 released UDS game "Ignition".

Have fun!
- Kiwi


================================================================
  Construction
================================================================

Polygons       : 1041 (Body, Axles, Shocks, Wheels)

Base           : Adeon by Acclaim
                 Get Air by Mighty Cucumber

Editors used   : Blender 2.79b
                 Ulead Photo Impact 12
                 Audacity
                 Notepad++


================================================================
  Thank You
================================================================

+ Mighty Cucumber for Get Air which I used as a base
+ UDS for the honk sound


================================================================
  Copyright / Permissions
================================================================

Please do not change any parts of this car without asking, as
long it's not for your personal use. You can reuse any parts of
this car for your own car, as long you mention the original
creators accordingly.
