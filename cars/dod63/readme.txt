================================================================
======================== Car Info ==============================
================================================================

Name				DOD-63
Author				Flyboy
Date				October 27, 2019
Type				Original
Folder Name			dod63
Rating				Semi-Pro
Class				Glow
Drivetrain			Four-wheel Drive
Top speed			32 mph
Body mass			1.2 kg

================================================================
======================= Description ============================
================================================================

Original car inspired by eighties' IMSA cars.


================================================================
======================== Credits ===============================
================================================================


Tools:
Gimp
Blender and Marv's plugin
Prm2Hul (By Jig)

================================================================
======================== Changelog =============================
================================================================
10/28/2019

Added an alternative skin

================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the authors wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.