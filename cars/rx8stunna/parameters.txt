{

;============================================================
;============================================================
; Spectra
;============================================================
;============================================================
Name		"Stunna"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\rx8stunna\body.prm"
MODEL 	1 	"cars\rx8stunna\wheel.prm"
MODEL 	2 	"NONE"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 	"cars\rx8stunna\car.bmp"
COLL 	"cars\rx8stunna\hull.hul"
;)TCARBOX  	"cars\rx8stunna\carbox.bmp" 			; Carbox texture
;)TSHADOW 	"cars\rx8stunna\shadow.bmp"
;)SHADOWINDEX 	-1 			; Use a default shadow (0 to 27 or -1)
;)SHADOWTABLE -49.5 49.6 82.5 -82.2 -8.5
EnvRGB 	80 80 80

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
;)CPUSelectable	true
;)Statistics 	true
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	2 			; Skill level (rookie, amateur, ...)
TopEnd     	2986.978242 			; Actual top speed (mph) for frontend bars
Acc        	4.839963 			; Acceleration rating (empirical)
Weight     	1.200000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	0 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	4.500000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	32.700000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 0.00000 4.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset
;)Flippable	false 		; Rotor car effect
;)Flying   	false 		; Flying like the UFO car
;)ClothFx  	false 		; Mystery car cloth effect

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0		; Calculated in game
Mass       	1.200000
Inertia    	1400.000000 0.000000 0.000000
           	0.000000 2200.000000 0.000000
           	0.000000 0.000000 700.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction  0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.0000 -3.50000 41.5000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.500000
EngineRatio 	10500.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	4400.000000
MaxPos      	4.000000
SkidWidth   	15.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.011000
StaticFriction  	1.800000
KineticFriction 	1.750000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	1
Offset1  	26.0000 -3.50000 41.50000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.500000
EngineRatio 	10500.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	4400.000000
MaxPos      	4.000000
SkidWidth   	15.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.011000
StaticFriction  	1.800000
KineticFriction 	1.750000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.00000 -3.500000 -38.50000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	1.000000
EngineRatio 	10500.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	4400.000000
MaxPos      	4.000000
SkidWidth   	15.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.011000
StaticFriction  	1.850000
KineticFriction 	1.800000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	1
Offset1  	26.00000 -3.500000 -38.50000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	1.000000
EngineRatio 	10500.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	4400.000000
MaxPos      	4.000000
SkidWidth   	15.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.011000
StaticFriction  	1.850000
KineticFriction 	1.800000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 -21.6.000000 50.000000
Axis        	0.000000 0.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-15.500000 -32.000000 -3.750000
Direction   	0.000000 -1.000000 0.000000
Length      	15.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial



;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	1043.510742
UnderRange  	2702.099854
UnderFront	 	2036.469604
UnderRear   	335.000000
UnderMax    	0.638043
OverThresh  	531.036438
OverRange   	1827.135498
OverMax     	0.640206
OverAccThresh  	613.148560
OverAccRange   	1021.179871
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}